package ru.simplex_software.wicket_springdata;

import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.IModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Persistable;
import ru.simplex_software.wicket_springdata.functions.SerializableFunction;

import java.io.Serializable;
import java.util.List;

/**
 * Отрисовщик элементов.
 */
public class PersistableChoiceRenderer<T extends Persistable<? extends Serializable>> implements IChoiceRenderer<T> {
    private static final Logger LOG = LoggerFactory.getLogger(PersistableChoiceRenderer.class);

    /**
     * Функция которая возращает отображаемое значение.
     */
    private SerializableFunction<T, String> function;

    public PersistableChoiceRenderer(SerializableFunction<T, String> function) {
        this.function = function;
    }

    @Override
    public Object getDisplayValue(T object) {
        return function.apply(object);
    }

    @Override
    public String getIdValue(T object, int index) {
        return object.getId().toString();
    }

    @Override
    public T getObject(String id, IModel<? extends List<? extends T>> choices) {
        for (T persistable : choices.getObject()) {
            if (id.equals(persistable.getId().toString())) {
                return persistable;
            }
        }
        LOG.warn("Persistable entity with id = {} not found.", id);
        return null;
    }
}
