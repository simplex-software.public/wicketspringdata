package ru.simplex_software.wicket_springdata.functions;

import java.io.Serializable;

/**
 * Обертка над операцией.
 * Проверяет обработчик на null при вызове.
 *
 * @param <T> тип аргумента.
 */
public class HandlerWrapper<T> implements Serializable {

    /**
     * Обработчик.
     */
    private SerializableConsumer<T> handler;

    /**
     * Установка обработчика.
     *
     * @param handler обработчик.
     */
    public void setHandler(SerializableConsumer<T> handler) {
        this.handler = handler;
    }

    /**
     * Вызов обработчика.
     *
     * @param tData аргумент.
     */
    public void fire(T tData) {
        if (handler != null) {
            handler.accept(tData);
        }
    }
}
