package ru.simplex_software.wicket_springdata.functions;

import java.io.Serializable;
import java.util.function.Consumer;

/**
 * Представляет сериализуемую операцию.
 *
 * @param <T> тип аргумента.
 * @see Consumer
 */
@FunctionalInterface
public interface SerializableConsumer<T> extends Consumer<T>, Serializable {
}
