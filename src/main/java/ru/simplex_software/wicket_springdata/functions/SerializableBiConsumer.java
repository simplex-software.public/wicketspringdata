package ru.simplex_software.wicket_springdata.functions;

import java.io.Serializable;
import java.util.function.BiConsumer;

/**
 * Представляет сериализуемую операцию выполняемую с 2 аргументами.
 *
 * @param <T> тип 1 аргумента.
 * @param <U> тип 2 аргумента.
 * @see BiConsumer
 */
@FunctionalInterface
public interface SerializableBiConsumer<T, U> extends BiConsumer<T, U>, Serializable {
}
