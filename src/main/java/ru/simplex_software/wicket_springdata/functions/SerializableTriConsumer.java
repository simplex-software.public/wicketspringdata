package ru.simplex_software.wicket_springdata.functions;

import java.io.Serializable;

/**
 * Представляет сериализуемую операцию выполняемую с 3 аргументами.
 *
 * @param <T> тип 1 аргумента.
 * @param <U> тип 2 аргумента.
 * @param <K> тип 3 аргумента.
 * @see ru.simplex_software.wicket_springdata.functions.TriConsumer
 */
@FunctionalInterface
public interface SerializableTriConsumer<T, U, K> extends TriConsumer<T, U, K>, Serializable {
}
