package ru.simplex_software.wicket_springdata.functions;

import java.io.Serializable;

/**
 * Обертка над операцией выполняемой с 3 аргументами.
 * Проверяет обработчик на null при вызове.
 *
 * @param <T> тип 1 аргумента.
 * @param <V> тип 2 аргумента.
 * @param <K> тип 3 аргумента.
 */
public class TriHandlerWrapper<T, V, K> implements Serializable {

    /**
     * Обработчик.
     */
    private SerializableTriConsumer<T, V, K> handler;

    /**
     * Установка обработчика.
     *
     * @param handler обработчик.
     */
    public void setHandler(SerializableTriConsumer<T, V, K> handler) {
        this.handler = handler;
    }

    /**
     * Вызов обработчика.
     *
     * @param tData 1 аргумент.
     * @param vData 2 аргумент.
     * @param kData 3 аргумент.
     */
    public void fire(T tData, V vData, K kData) {
        if (handler != null) {
            handler.accept(tData, vData, kData);
        }
    }
}
