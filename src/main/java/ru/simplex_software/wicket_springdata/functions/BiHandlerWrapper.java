package ru.simplex_software.wicket_springdata.functions;

import java.io.Serializable;

/**
 * Обертка над операцией выполняемой с 2 аргументами.
 * Проверяет обработчик на null при вызове.
 *
 * @param <T> тип 1 аргумента.
 * @param <V> тип 2 аргумента.
 */
public class BiHandlerWrapper<T, V> implements Serializable {

    /**
     * Обработчик.
     */
    private SerializableBiConsumer<T, V> handler;

    /**
     * Установка обработчика.
     *
     * @param handler обработчик.
     */
    public void setHandler(SerializableBiConsumer<T, V> handler) {
        this.handler = handler;
    }

    /**
     * Вызов обработчика.
     *
     * @param tData 1 аргумент.
     * @param vData 2 аргумент.
     */
    public void fire(T tData, V vData) {
        if (handler != null) {
            handler.accept(tData, vData);
        }
    }
}
