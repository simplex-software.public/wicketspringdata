package ru.simplex_software.wicket_springdata.functions;

/**
 * Представляет операцию выполняемую с аргументами.
 *
 * @param <T> тип 1 аргумента.
 * @param <U> тип 2 аргумента.
 * @param <K> тип 3 аргумента.
 */
@FunctionalInterface
public interface TriConsumer<T, U, K> {

    /**
     * Выполнение операции.
     *
     * @param t 1 аргумент.
     * @param u 2 аргумент.
     * @param k 3 аргумент.
     */
    void accept(T t, U u, K k);
}
