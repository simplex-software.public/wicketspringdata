package ru.simplex_software.wicket_springdata.functions;

import java.io.Serializable;
import java.util.function.Function;

/**
 * Представляет сериализуемую функцию.
 *
 * @param <T> тип аргумента.
 * @param <R> тип возвращаемого значения.
 * @see java.util.function.Function
 */
@FunctionalInterface
public interface SerializableFunction<T, R> extends Function<T, R>, Serializable {
}
