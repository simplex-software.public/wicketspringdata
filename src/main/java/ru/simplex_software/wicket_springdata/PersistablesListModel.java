package ru.simplex_software.wicket_springdata;

import org.apache.wicket.model.LoadableDetachableModel;
import org.springframework.data.domain.Persistable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Hibernate Model списка.
 */
public class PersistablesListModel<P extends Persistable<? extends Serializable>> extends LoadableDetachableModel<List<P>> {

    /**
     * Список моделей.
     */
    private List<HibernateModel<P>> modelList;

    public PersistablesListModel(List<P> list) {
        super(list);
        modelList = new ArrayList<>(list.size());
        for (P pe : list) {
            modelList.add(new HibernateModel<>(pe));
        }
    }

    @Override
    public void onDetach() {
        for (HibernateModel obj : modelList) {
            obj.detach();
        }
    }

    @Override
    public void setObject(List<P> object) {
        detach();
        super.setObject(object);
        modelList = new ArrayList<>(object.size());
        for (P pe : object) {
            modelList.add(new HibernateModel<>(pe));
        }
    }

    @Override
    protected List<P> load() {
        List<P> list = new ArrayList<>(this.modelList.size());
        for (HibernateModel<P> hibernateModel : this.modelList) {
            hibernateModel.load();
            list.add(hibernateModel.getObject());
        }
        return list;
    }

    public Iterator<HibernateModel<P>> getItegator() {
        return modelList.iterator();
    }
}
