package ru.simplex_software.wicket_springdata.kryo;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import org.springframework.data.domain.Persistable;

import java.io.Serializable;

/**
 * Загрузка и настройка Kryo.
 */
public abstract class AbstractKryoHolder {

    protected Kryo kryo = new Kryo();

    /**
     * Сериализация объекта.
     */
    public <P extends Persistable<? extends Serializable>> void writeObject(Output output, P entity) {
        kryo.writeObject(output, entity);
    }

    /**
     * Десериализация объекта.
     */
    public <P extends Persistable<? extends Serializable>> P readObject(Input input, Class<P> type) {
        return kryo.readObject(input, type);
    }
}