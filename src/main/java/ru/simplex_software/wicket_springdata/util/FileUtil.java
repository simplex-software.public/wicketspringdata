package ru.simplex_software.wicket_springdata.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Вспомогательный класс для работы с файлами.
 */
public class FileUtil {
    public static Logger LOG = LoggerFactory.getLogger(FileUtil.class);

    /**
     * Получение файла по имени.
     *
     * @param filename имя файла.
     * @return файл.
     */
    public static File getFile(String filename, String FOLDER) {
        return new File(FOLDER + filename);
    }

    /**
     * Сохранение файла.
     *
     * @param filename имя файла.
     * @param data     данные файла.
     */
    public static void saveFile(String filename, byte[] data, String FOLDER) {
        try {
            File file = new File(FOLDER + filename);
            FileUtils.writeByteArrayToFile(file, data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Получение массива байт из файла.
     *
     * @param filename имя файла.
     * @return массив байт файла.
     */
    public static byte[] getFileBytes(String filename, String FOLDER) {
        try (InputStream inputStream = new FileInputStream(new File(FOLDER + filename))) {
            return IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Удаление файла.
     *
     * @param filename имя файла.
     */
    public static void deleteFile(String filename, String FOLDER) {
        try {
            Files.delete(Paths.get(FOLDER + filename));
        } catch (IOException e) {
            LOG.warn("Cannot delete file {}: {}", filename, e.getMessage());
        }
    }

    /**
     * Запись файла в поток.
     *
     * @param file     файл.
     * @param outputStream поток для записи.
     */
    public static void writeFileToStream(File file, OutputStream outputStream) {
        try {
            Path path = file.toPath();
            Files.copy(path, outputStream);
            outputStream.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}