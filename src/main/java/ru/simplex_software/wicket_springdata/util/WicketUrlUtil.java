package ru.simplex_software.wicket_springdata.util;

import org.apache.wicket.core.request.handler.BookmarkablePageRequestHandler;
import org.apache.wicket.core.request.handler.PageProvider;
import org.apache.wicket.request.IRequestHandler;
import org.apache.wicket.request.IRequestMapper;
import org.apache.wicket.request.Url;
import org.apache.wicket.request.component.IRequestablePage;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Вспомогательный класс для работы с URL.
 */
public final class WicketUrlUtil {

    public static final Logger LOG = LoggerFactory.getLogger(WicketUrlUtil.class);

    private WicketUrlUtil() {
        throw new IllegalStateException("");
    }

    /**
     * Создание URL для страницы с параметрами.
     *
     * @param pageClass  класс страницы.
     * @param parameters параметры.
     * @return URL страницы с параметрами.
     */
    public static String createPageWithParametersUrl(Class<? extends IRequestablePage> pageClass,
                                                     IRequestMapper mapper, PageParameters parameters) {
        Url url = RequestCycle.get().getRequest().getUrl();
        String pageUrl = getPageUrl(pageClass, mapper);
        StringBuilder builder = new StringBuilder();

        builder.append(url.getProtocol()).append("://").append(url.getHost());

        String protocol = url.getProtocol();
        Integer port = url.getPort();
        if ((protocol.equals("http") && port.equals(80))
                || (protocol.equals("https") && port.equals(443))) {
            builder.append("/").append(pageUrl);
        } else {
            builder.append(":").append(url.getPort()).append("/").append(pageUrl);
        }

        // Логгируем протокол и полный url заявки, из которого excel формирует гипер-ссылку для перехода по ctrl+click
        LOG.debug("Protocol: " + url.getProtocol());
        LOG.debug("Link to request: " + builder.toString());

        // Добавление параметров.
        if (!parameters.isEmpty()) {
            builder.append("?");
        }

        String prefix = "";
        for (String key : parameters.getNamedKeys()) {
            builder.append(prefix).append(key).append("=").append(parameters.get(key));
            prefix = "&";
        }

        return builder.toString();
    }

    /**
     * Получение URL страницы по её классу.
     *
     * @param pageClass класс страницы.
     * @return URL страницы.
     */
    private static String getPageUrl(Class<? extends IRequestablePage> pageClass, IRequestMapper mapper) {
        IRequestHandler handler = new BookmarkablePageRequestHandler(new PageProvider(pageClass));
        return mapper.mapHandler(handler).toString();
    }
}
