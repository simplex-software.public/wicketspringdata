package ru.simplex_software.wicket_springdata;

import org.apache.wicket.model.IModel;
import org.springframework.data.domain.Persistable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Модель списка, элементы которого обернуты в модели.
 *
 * @param <P> тип элементов списка.
 */
public class HibernateListModel<P extends Persistable<? extends Serializable>> implements IModel<List<P>> {

    /**
     * Список моделей элементов.
     */
    private List<IModel<P>> modelList;

    /**
     * Загружена ли модель.
     */
    private boolean attached;

    /**
     * Создание новой модели списка из итерируемого объекта.
     *
     * @param iterable итерируемый объект.
     */
    public HibernateListModel(Iterable<P> iterable) {
        this(StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList()));
    }

    /**
     * Создание новой модели списка.
     */
    public HibernateListModel() {
        this(new ArrayList<>());
    }

    /**
     * Создание новой модели списка из списка.
     *
     * @param list список элементов.
     */
    public HibernateListModel(List<P> list) {
        this.modelList = wrap(list);
        attached = true;
    }

    /**
     * Получение списка элементов.
     */
    @Override
    public List<P> getObject() {
        checkLoad();
        return unwrap(modelList);
    }

    /**
     * Замена списка моделей.
     *
     * @param list список объектов.
     */
    @Override
    public void setObject(List<P> list) {
        checkLoad();
        modelList = wrap(list);
    }

    /**
     * Открепление модели.
     */
    @Override
    public void detach() {
        modelList.forEach(IModel::detach);
        attached = false;
    }

    /**
     * Добавление модели элемента в список.
     *
     * @param model модель элемента.
     * @return результат операции.
     */
    public boolean add(IModel<P> model) {
        return add(model.getObject());
    }

    /**
     * Добавление элемента в список.
     *
     * @param item элемент.
     * @return результат операции.
     */
    public boolean add(P item) {
        checkLoad();
        return modelList.add(new HibernateModel<>(item));
    }

    /**
     * Удаление модели элемента из списока.
     *
     * @return результат операции.
     */
    public boolean remove(IModel<P> model) {
        return remove(model.getObject());
    }

    /**
     * Удаление элемента из списока.
     *
     * @return результат операции.
     */
    public boolean remove(P item) {
        checkLoad();
        return modelList.remove(new HibernateModel<>(item));
    }

    /**
     * Удаление из списка всех элементов которые есть в коллекции.
     *
     * @param collection колекция элементов.
     * @see List#removeAll(Collection)
     */
    public void removeAll(Collection<P> collection) {
        checkLoad();
        modelList.removeIf(model -> collection.contains(model.getObject()));
    }

    /**
     * Удаление из списка всех элементов которых нет в коллекции.
     *
     * @param collection колекция элементов.
     * @see List#retainAll(Collection)
     */
    public void retainAll(Collection<P> collection) {
        checkLoad();
        modelList.removeIf(model -> !collection.contains(model.getObject()));
    }

    /**
     * Получение итератора списка моделей элементов.
     */
    public Iterator<IModel<P>> getIterator() {
        checkLoad();
        return modelList.iterator();
    }

    /**
     * Проверка наличия модели в списке.
     *
     * @param model список моделей.
     * @return наличие модели в списке.
     */
    public boolean contains(IModel<P> model) {
        checkLoad();
        return modelList.contains(model);
    }

    /**
     * Проверка наличия элемента в списке.
     *
     * @param item элемент.
     * @return наличие элемента в списке.
     */
    public boolean contains(P item) {
        return contains(new HibernateModel<>(item));
    }

    /**
     * Очистка модели.
     */
    public void clear() {
        checkLoad();
        modelList.clear();
    }

    /**
     * Оборачивание элементов списка в модели.
     *
     * @param list список элементов.
     * @return список моделей элементов.
     */
    private List<IModel<P>> wrap(List<P> list) {
        return list.stream()
                .map(HibernateModel::new)
                .map(model -> (IModel<P>) model)
                .collect(Collectors.toList());
    }

    /**
     * Получение списка элементов.
     *
     * @param modelList список моделей элементов.
     * @return список элементов.
     */
    private List<P> unwrap(List<IModel<P>> modelList) {
        return modelList.stream()
                .map(IModel::getObject)
                .collect(Collectors.toList());
    }

    /**
     * Проверка загрузки и загрузка при необходимости.
     */
    private void checkLoad() {
        if (!attached) {
            modelList.forEach(model -> ((HibernateModel<P>) model).load());
            attached = true;
        }
    }
}
