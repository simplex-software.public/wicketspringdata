package ru.simplex_software.wicket_springdata.property;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.wicket.Component;
import org.apache.wicket.application.IComponentInstantiationListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.function.Function;

/**
 * Внедряет значения из property файлов в поля, помеченные аннотацией {@link Property}.
 */
public class WicketPropertyInjector implements IComponentInstantiationListener {
    private static final Logger LOG = LoggerFactory.getLogger(WicketPropertyInjector.class);

    /**
     * Находит значение свойства по названию.
     */
    private Function<String, String> propertyResolver;

    public WicketPropertyInjector(Function<String, String> propertyResolver) {
        this.propertyResolver = propertyResolver;
    }

    @Override
    public void onInstantiation(Component component) {
        injectProperties(component);
    }

    /**
     * Внедрение свойств в поля компонента.
     *
     * @param component компонент.
     */
    private void injectProperties(Component component) {
        List<Field> fields = FieldUtils.getFieldsListWithAnnotation(component.getClass(), Property.class);
        for (Field field : fields) {
            // Проверка типа поля.
            if (!field.getType().equals(String.class)) {
                throw new IllegalArgumentException("Annotated field " + field.getName() + " must have type java.lang.String");
            }
            // Получение названия и проверка наличия свойства.
            String aValue = getPropertyName(field);
            String pValue = propertyResolver.apply(aValue);
            if (pValue == null) {
                String message = String.format("Cannot find property %s for %s", aValue, field.getName());
                throw new IllegalArgumentException(message);
            }
            // Установка значения.
            try {
                field.setAccessible(true);
                field.set(component, pValue);
                field.setAccessible(false);
            } catch (IllegalAccessException e) {
                LOG.warn("Cannot set property");
            }
        }
    }

    /**
     * Получение названия свойства.
     *
     * @param field поле.
     * @return название свойства.
     */
    private String getPropertyName(Field field) {
        String name = field.getAnnotation(Property.class).value();
        if (StringUtils.isEmpty(name)) {
            name = field.getName();
        }
        return name;
    }
}
